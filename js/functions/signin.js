$('#submit_signin').on('click', function() {
    $email = $('#email').val();
    $password = $('#password').val();
 
    if ($email == '' || $password == '')
    {
        $('#formSignin .alert').removeClass('hidden');
        $('#formSignin .alert').html('Please complete the above information.');
    }
    else
    {
        $.ajax({
            url : 'signin.php',
            type : 'POST',
            data : {
                email : $email,
                password : $password
            }, success : function(data) {
                $('#formSignin .alert').html(data);
            }
        });
    }
});