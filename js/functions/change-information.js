$('#submit_change_information').on('click', function() {
    $name = $('#name').val();
    $birthday = $('#birthday').val();
    $sex = $('#sex').val();
    $email = $('#email').val();
 
    if ($name == '' || $birthday == '' ||  $email == '')
    {
        $('#formChangePass .alert').removeClass('hidden');
        $('#formChangePass .alert').html('Please complete the above information!');
    }
    else
    {
        $.ajax({
            url : 'change-information.php',
            type : 'POST', 
            data : {
                name : $name,
                birthday : $birthday,
                sex : $sex,
                email : $email
            }, success : function(data) {
                $('#formChangePass .alert').removeClass('hidden');
                $('#formChangePass .alert').html(data);
            }
        });
    }
});