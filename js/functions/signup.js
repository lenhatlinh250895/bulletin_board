$('#submit_signup').on('click', function() {
    $name_signup = $('#name_signup').val();
    $birthday_signup = $('#birthday_signup').val();
    $sex_signup = $('#sex_signup').val();
    $email_signup = $('#email_signup').val();
    $pass_signup = $('#pass_signup').val();
    $repass_signup = $('#repass_signup').val();
    // alert($repass_signup);
    // alert($pass_signup);
    //return false;
    if ($name_signup == '' || $birthday_signup == '' || $email_signup == '' || $pass_signup == '' || $repass_signup == '')
    {
        $('#formSignup .alert').removeClass('hidden');
        $('#formSignup .alert').html('Please complete the above information.');
    }
    else
    {
        $.ajax({
            url : 'signup.php', 
            type : 'POST', 
            data : {
                name_signup : $name_signup,
                birthday_signup : $birthday_signup,
                sex_signup : $sex_signup,
                email_signup : $email_signup,
                pass_signup : $pass_signup,
                repass_signup : $repass_signup
            }, success : function(data) {
                $('#formSignup .alert').html(data);
            }
        });
    }
});