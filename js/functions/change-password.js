$('#submit_change_pass').on('click', function() {
    $old_pass = $('#old_pass').val();
    $new_pass = $('#new_pass').val();
    $re_new_pass = $('#re_new_pass').val();
 
    if ($old_pass == '' || $old_pass == '' || $re_new_pass == '')
    {
        $('#formChangePass .alert').removeClass('hidden');
        $('#formChangePass .alert').html('Please complete the above information!');
    }
    else
    {
        $.ajax({
            url : 'change-password.php',
            type : 'POST',
            data : {
                old_pass : $old_pass,
                new_pass : $new_pass,
                re_new_pass : $re_new_pass
            }, success : function(data) {
                $('#formChangePass .alert').removeClass('hidden');
                $('#formChangePass .alert').html(data);
                if(data == '')
                    location.replace("http://localhost/bulletin_board/index.php");
            }
        });
    }
});