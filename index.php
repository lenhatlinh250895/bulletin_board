<?php
 
require_once 'core/init.php';


$sql_get_date_permission = "SELECT * FROM `permission`";
$permission = $db->fetch_assoc($sql_get_date_permission,1);
if($permission['permission'] == 0)
{
    if ($email)
    {
        if (isset($_GET['ac']))
        {
            $ac = addslashes(trim(htmlentities($_GET['ac'])));
            if ($ac == 'information')
            {
                require_once 'templates/information.php';
            }
            else if ($ac == 'edit')
            {
                require_once 'templates/editinformation.php';
            }
            else if ($ac == 'changepass')
            {
                require_once 'templates/change-pass-form.php';
            }
            else if ($ac == 'createpost')
            {
                require_once 'templates/create-post-form.php';
            }
            else if ($ac == 'mypost')
            {
                require_once 'templates/my-post-form.php';
            }
            else if ($ac == 'update')
            {
                require_once 'templates/update-post-form.php';
            }
            else if ($ac == 'detail')
            {
                require_once 'templates/detail-form.php';
            }
            else if ($ac == 'listuser')
            {
                require_once 'templates/list-user-form.php';
            }
            
        }
        else
        {
            require_once 'templates/index.php';
        }
    }
    else
    {   
        require_once 'includes/header.php';
        require_once 'templates/signin-up-form.php';
        require_once 'includes/footer.php';
    }
}
else
{
    if ($email)
    {
        if (isset($_GET['ac']))
        {
            $ac = addslashes(trim(htmlentities($_GET['ac'])));
            if ($ac == 'information')
            {
                require_once 'templates/information.php';
            }
            else if ($ac == 'edit')
            {
                require_once 'templates/editinformation.php';
            }
            else if ($ac == 'changepass')
            {
                require_once 'templates/change-pass-form.php';
            }
            else if ($ac == 'createpost')
            {
                require_once 'templates/create-post-form.php';
            }
            else if ($ac == 'mypost')
            {
                require_once 'templates/my-post-form.php';
            }
            else if ($ac == 'update')
            {
                require_once 'templates/update-post-form.php';
            }
            else if ($ac == 'detail')
            {
                require_once 'templates/detail-form.php';
            }
            else if ($ac == 'listuser')
            {
                require_once 'templates/list-user-form.php';
            }
            
        }
        else
        {
            require_once 'templates/index.php';
        }
    }
    else
    {   
        if (isset($_GET['ac']))
        {
            $ac = addslashes(trim(htmlentities($_GET['ac'])));
            if ($ac == 'index')
            {
                require_once 'templates/index.php';
            }
            else if ($ac == 'createpost')
            {
                require_once 'templates/create-post-form.php';
            }
            else if ($ac == 'detail')
            {
                require_once 'templates/detail-form.php';
            }        
        }
        else
        {
            require_once 'includes/header.php';
            require_once 'templates/signin-up-form.php';
            require_once 'includes/footer.php';
            //require_once 'templates/index.php';
        }
        
    }
}

 
?>