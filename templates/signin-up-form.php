<?php 

$sql_get_date_permission = "SELECT * FROM `permission`";
$permission = $db->fetch_assoc($sql_get_date_permission,1);

?>

<div class="container">
    <div class="formlogin row">
        <div class="col-md-5">
            <h3 class="text-primary">Login</h3>
            <form method="POST" onsubmit="return false;" id="formSignin">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password">
                </div>
                <button class="btn btn-primary" id="submit_signin">Login</button>
                <button class="btnsignup btn btn-secondary" id="signup">Signup</button>
                <?php 
                    if($permission['permission'] == 1)
                        echo '<a href="index.php?ac=index">Anonymous access</a>';
                ?>
                <br><br>
                <div class="alert alert-danger hidden"></div>
            </form>
        </div>
    </div>
    <div class="formsignup row hidden">
        <div class="col-md-5">
            <h3 class="text-success">Sign Up</h3>
            <p class="text-danger">* Please enter the following information to register for an account :</p>
            <form method="POST" onsubmit="return false;" id="formSignup">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name" id="name_signup">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Birthday" id="birthday_signup"> Ex:1995-2-30
                </div>
                <div class="form-group">
                    <label for="sex">Sex</label>
                    <select id="sex_signup">
                        <option value="1">Male</option>}
                        <option value="0">Female</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" id="email_signup">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" id="pass_signup">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Re Password" id="repass_signup">
                </div>
                <button class="btn btn-success" id="submit_signup">Create Account</button>
                <button class="btnlogin btn btn-primary" id="btnlogin">Login</button>
                <br><br>
                <div class="alert alert-danger hidden"></div>
            </form>
        </div>
    </div>
</div>
<script src="js/jquery.js"></script>
<script type="text/javascript">
    $('.btnsignup').on('click',function(){
        $('.formsignup').removeClass('hidden');
        $('.formlogin').addClass('hidden');
    });
    $('.btnlogin').on('click',function(){
        $('.formsignup').addClass('hidden');
        $('.formlogin').removeClass('hidden');
    });
</script>