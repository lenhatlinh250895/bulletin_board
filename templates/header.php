<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<link rel="alternate" type="application/rss+xml" title="RSS" href="https://xoops.ec-cube.net/modules/rssj/rss.php" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="content-language" content="ja" />
<meta name="robots" content="index,follow" />
<meta name="rating" content="general" />
<meta name="generator" content="XOOPS" />
<title>Bulletin Board-</title>
<link rel="stylesheet" type="text/css" media="screen" href="https://xoops.ec-cube.net/xoops.css" />
<link rel="stylesheet" type="text/css" media="screen" href="https://xoops.ec-cube.net/themes/default/styleNN.css" />
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
</head>
<body>
  <div class="blockContent">
    <div class="top-forum-menu">
      <?php 
        $sql_get_date_permission = "SELECT * FROM `permission`";
        $permission = $db->fetch_assoc($sql_get_date_permission,1);
        if($permission['permission'] == 1 && !isset($data_user))
          echo '
        <a class="menuTop" href="index.php?ac=index"><span class="menuMain">Post</span></a>
        <a class="menuTop" href="index.php?ac=createpost"><span class="menuMain">Create Post</span></a>';
        else
          echo '
        <a class="menuTop" href="index.php?ac=information"><span class="menuMain">My information</span></a>
        <a class="menuTop" href="index.php"><span class="menuMain">Post</span></a>
        <a class="menuTop" href="index.php?ac=createpost"><span class="menuMain">Create Post</span></a>
        <a class="menuTop" href="index.php?ac=mypost"><span class="menuMain">My Post</span></a>';
          
      ?>
      <?php if(isset($data_user)) if($data_user['level'] == 1) 
      echo '
      <a class="menuTop" href="index.php?ac=listuser"><span class="menuMain">User List</span></a>';
      ?>
    </div>
  </div>