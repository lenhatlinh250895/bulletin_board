<?php include_once 'header.php'; ?>
  <table cellspacing="0" width="780">
    <tr>
      <td id="leftcolumn">
        <h3 class="blockTitle">Category</h3>
          <div class="blockContent">
            <table cellspacing="0">
              <tbody>
                <tr>
                  <td class="forummenu">
                    <a class="menuTop" href="">PHP</a>
                    <a class="menuTop" href="">C#</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>                  
</div></div>                <!-- End left blocks loop -->
      </td>

      <td id="centercolumn">

        <!-- Display center blocks if any -->

        
        <table cellspacing="0">
          <tr>
            <td id="centerCcolumn" colspan="2">

            <!-- Start center-center blocks loop -->
                          <div style="padding: 15px 15px 10px;">
    <div class="blockContent"><table border="0" cellspacing="0" cellpadding="0" style="background-color: #dfdfdf;">
	<tbody>
</tbody>
</table>
<!--div class="banner" style="text-align: center;margin-top: 50px">
</div -->
</div>
</div>
                          <div style="padding: 0 15px 10px;">
    <div class="blockContent">
      <div class="row">
        <div class="col-md-12">
            <h3 class="text-primary">Change Post</h3>
            <?php
              $id_post = $_GET['id_post'];
              $sql_get_data_post = "SELECT * FROM post WHERE id_post = '$id_post'";
              $data = $db->fetch_assoc($sql_get_data_post,1);
            ?>
            <form method="POST" action="update-post.php" id="updatepost">
                <input type="hidden" name="id_post" value="<?php echo $_GET['id_post']; ?>">
                <div class="form-group">
                    <label for="thread">Thread</label>
                    <input type="text" class="form-control" name="thread" value="<?php echo $data['thread']; ?>" >
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <input type="text" class="form-control" name="content" value="<?php echo $data['content']; ?>" >
                </div>
                <div class="form-group">
                    <label for="category">Category</label>
                    <select name="category" id="category">
                      <?php 
                        $sql_get_data_category = "SELECT * FROM `category`";
                        foreach ($db->fetch_assoc($sql_get_data_category, 0) as $key => $data_list_category)
                        {
                          $selected = '';
                          if($data['id_category'] == $data_list_category['id_category'])
                            $selected = 'selected=selected';
                          echo '<option '.$selected.' value="'.$data_list_category['id_category'].'" >'.$data_list_category['name_category'].'</option>}';
                        }
                      ?>
                    </select>
                </div>
                <a href="index.php" class="btn btn-default">
                    <span class="glyphicon glyphicon-arrow-left"></span> Come Back
                </a>
                <!-- <button class="btn btn-primary" id="submit_create_post">
                    <span class="glyphicon glyphicon-ok"></span> Create
                </button> -->
                <input class="btn btn-primary" type="submit" name="create" value="create">
                <br><br>
                <div class="alert alert-danger hidden"></div>
            </form>
        </div>
<style type="text/css">
/* Facebook Like Box width:100% */
.fbcomments,
.fb_iframe_widget,
.fb_iframe_widget[style],
.fb_iframe_widget iframe[style],
.fbcomments iframe[style],
.fb_iframe_widget span{
    width: 100% !important;
}
</style>
</div></div>
</div>
                        <!-- End center-center blocks loop -->

            </td>
          </tr>
          <tr>
            <td id="centerLcolumn">

            <!-- Start center-left blocks loop -->
                          <!-- End center-left blocks loop -->

            </td><td id="centerRcolumn">

            <!-- Start center-right blocks loop -->
                          <!-- End center-right blocks loop -->

            </td>
          </tr>
        </table>

                <!-- End display center blocks -->

        <div id="content">
          
        </div>
      </td>

      
      <td id="rightcolumn">
<a href="signout.php">
  <span class="glyphicon glyphicon-off"></span> Logout
</a>
<?php 
  $sql_get_date_permission = "SELECT * FROM `permission`";
  $permission = $db->fetch_assoc($sql_get_date_permission,1);
  if($data_user['level'] == 1)
  {
    if($permission['permission'] == 1)
      echo '<a href="change-permission.php?per=0"><input type="button" class="btn btn-info" value="Not allow access"></a>';
    else
      echo '<a href="change-permission.php?per=1"><input type="button" class="btn btn-primary" value="Allow access"></a>';
  }
  if($data_user['level'] == 1) { 
?>
<ul class="navbar-nav ml-auto">
  <!-- Notifications Dropdown Menu -->
  <li class="nav-item dropdown">
    <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
      <span class="badge badge-warning navbar-badge">Notification</span>
    </a>
    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
      <?php 
        $sql_get_data_notification = "SELECT * FROM `notification` ORDER BY `id_notification` DESC LIMIT 5";
        foreach($db->fetch_assoc($sql_get_data_notification,0) as $key => $data_list_notification)
        {
          $day_1 = $data_list_notification['action_datetime'];
          $day_2 = date('Y-m-d H:i:s') ;
          $days = abs(strtotime($day_2) - strtotime($day_1));
          $year = floor($days / (365*60*60*24));  
          $month = floor(($days - $year * 365*60*60*24) / (30*60*60*24));  
          $day = floor(($days - $year * 365*60*60*24 - $month*30*60*60*24)/ (60*60*24));  
          $h  = floor(($days - $year * 365*60*60*24 - $month*30*60*60*24 - $day*60*60*24)/(60*60));  
          $i  = floor(($days - $year * 365*60*60*24 - $month*30*60*60*24 - $day*60*60*24 - $h*60*60)/(60)); 
          if($year != 0)
            $time = $year.' year';
          else if($month != 0)
            $time = $month.' month';
          else if($day != 0)
            $time = $day.' day';
          else if($h != 0)
            $time = $h.' hours';
          else 
            $time = $i.' minute';
          echo ' 
          <div class="dropdown-divider"></div>
            <i class="fas fa-envelope mr-2"></i> '.$data_list_notification['thread'].'
            <span class="float-right text-muted text-sm">'.$time.'</span>';
          if($data_list_notification['status'] == 0)
          echo 
               '<div><a style="color: red;" href="accept.php?action='.$data_list_notification['action'].'&id_post='.$data_list_notification['id_post'].'&id_notification='.$data_list_notification['id_notification'].'" >Accept</a></div>';
        }
      ?>

    </div>
  </li>
</ul>
<?php } ?>
</div></div>
</div>
      </td>

          </tr>
  </table>
<?php include_once 'footer.php'; ?>
