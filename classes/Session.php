<?php 

class Session {

    public function send($email)
    {
        $_SESSION['email'] = $email;
    }

    public function start()
    {
        session_start();
    }

    public function get()
    {
        if(isset($_SESSION['email']))
        {
            $email = $_SESSION['email'];
        }
        else
        {
            $email = '';
        }
        return $email;
    }

    public function destroy()
    {
        session_destroy();
    }

}

?>