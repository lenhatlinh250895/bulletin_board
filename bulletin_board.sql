-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 03, 2020 at 03:51 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bulletin_board`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `name_category` varchar(255) NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id_category`, `name_category`) VALUES
(1, 'PHP'),
(2, 'C#');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `sex` int(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_member`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `name`, `birthday`, `sex`, `email`, `password`, `level`) VALUES
(3, 'Nhat', '2020-02-02', 1, 'lenhatlinh25081995@gmail.com', 'eced110fa1737081f2ea67d875118c62', 0),
(2, 'Nam Nhat', '2019-10-16', 1, 'lenhatlinh250895@gmail.com', 'eced110fa1737081f2ea67d875118c62', 0),
(4, 'admin', '2020-03-02', 1, 'admin@gmail.com', 'eced110fa1737081f2ea67d875118c62', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `id_notification` int(11) NOT NULL AUTO_INCREMENT,
  `thread` varchar(255) NOT NULL,
  `action` int(11) NOT NULL COMMENT '1 create// 2 delete',
  `id_member` int(11) DEFAULT NULL,
  `id_post` int(11) NOT NULL,
  `action_datetime` datetime NOT NULL,
  `status` int(11) DEFAULT 0,
  PRIMARY KEY (`id_notification`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id_notification`, `thread`, `action`, `id_member`, `id_post`, `action_datetime`, `status`) VALUES
(16, 'admin create one new post!', 1, 4, 672, '2020-03-02 11:39:11', 1),
(17, 'admin delete one post!', 2, 4, 672, '2020-03-02 13:20:14', 1),
(18, 'admin create one new post!', 1, 4, 673, '2020-03-02 13:21:30', 1),
(20, 'admin create one new post!', 1, 4, 674, '2020-03-02 13:28:35', 1),
(21, 'Nam create one new post!', 1, 2, 675, '2020-03-02 13:28:57', 1),
(22, 'admin delete one post!', 2, 4, 674, '2020-03-02 13:31:25', 1),
(23, 'admin create one new post!', 1, 4, 676, '2020-03-02 13:37:34', 1),
(24, 'admin delete one post!', 2, 4, 676, '2020-03-02 13:37:46', 1),
(25, 'Anonymous create one new post!', 1, NULL, 678, '2020-03-02 14:52:45', 1),
(26, 'admin delete one post!', 2, 4, 678, '2020-03-02 14:54:08', 1),
(27, 'admin1 delete one post!', 2, 4, 675, '2020-03-02 15:01:37', 1),
(28, 'admin1 create one new post!', 1, 4, 679, '2020-03-02 15:02:02', 1),
(29, 'Anonymous create one new post!', 1, NULL, 680, '2020-03-02 15:05:00', 1),
(30, 'Nam Nhat create one new post!', 1, 2, 681, '2020-03-02 15:07:02', 1),
(31, 'Anonymous create one new post!', 1, NULL, 682, '2020-03-02 15:07:40', 1),
(32, 'Anonymous create one new post!', 1, NULL, 683, '2020-03-02 15:38:27', 1),
(33, 'admin create one new post!', 1, 4, 684, '2020-03-02 16:52:32', 1),
(37, 'admin delete one post!', 2, 4, 684, '2020-03-02 17:15:10', 1),
(38, 'admin create one new post!', 1, 4, 687, '2020-03-02 17:15:22', 1),
(39, 'admin delete one post!', 2, 4, 683, '2020-03-03 08:29:16', 1),
(40, 'Nam Nhat delete one post!', 2, 2, 681, '2020-03-03 08:56:35', 1),
(42, 'admin delete one post!', 2, 5, 682, '2020-03-03 09:44:02', 1),
(43, 'admin delete one post!', 2, 5, 680, '2020-03-03 09:44:22', 1),
(44, 'Nam Nhat delete one post!', 2, 2, 667, '2020-03-03 09:44:45', 1),
(45, 'admin create one new post!', 1, 5, 688, '2020-03-03 10:15:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
CREATE TABLE IF NOT EXISTS `permission` (
  `id_permission` int(1) NOT NULL AUTO_INCREMENT,
  `permission` int(1) NOT NULL,
  PRIMARY KEY (`id_permission`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id_permission`, `permission`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `id_post` int(11) NOT NULL AUTO_INCREMENT,
  `thread` varchar(255) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `status` int(11) DEFAULT 0,
  `date_submitted` datetime NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_member` int(11) DEFAULT NULL,
  `viewmode` int(11) DEFAULT 0 COMMENT '1: all view // 0: member view',
  PRIMARY KEY (`id_post`)
) ENGINE=MyISAM AUTO_INCREMENT=689 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id_post`, `thread`, `content`, `status`, `date_submitted`, `id_category`, `id_member`, `viewmode`) VALUES
(2, 'Study C#', 'I want to study C#', 1, '2020-02-28 00:00:00', 2, 2, 0),
(3, 'asdasdsad', '123123213213', 1, '2020-02-02 00:00:00', 1, 2, 0),
(687, 'test moi', 'zxcqweqe1212', 1, '2020-03-02 17:15:22', 1, 4, 1),
(671, 'Frameword laravel 4x', 'I study frameword laravel for ....', 1, '2020-03-02 11:37:22', 2, 4, 0),
(668, 'Hoc ma choi, choi ma hoc', 'I study frameword laravel for ....', 1, '2020-03-02 11:24:02', 2, 4, 0),
(669, 'Frameword laravel 4x', 'Chung ta cung nhau tham gia 1 lop hoc vui nhon nao.', 1, '2020-03-02 11:32:36', 1, 4, 1),
(670, 'admin create one new post!', 'Chung ta cung nhau tham gia 1 lop hoc vui nhon nao.', 1, '2020-03-02 11:35:35', 2, 4, 1),
(672, 'Frameword laravel 4x', 'Chung ta cung nhau tham gia 1 lop hoc vui nhon nao.', 1, '2020-03-02 11:39:11', 2, 4, 0),
(679, 'le nhat linh', 'zxcaqeqwe', 1, '2020-03-02 15:02:02', 1, 4, 1),
(677, 'le nhat linh', 'le nhat linh 123', 1, '2020-03-02 14:48:20', 1, NULL, 1),
(688, 'Ã¡dasdsadsad', 'zxczxczxcxzc', 1, '2020-03-03 10:15:21', 1, 4, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
